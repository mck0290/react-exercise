/** @jsxImportSource @emotion/react */
import React from 'react';
import axios from "axios";
import { STATES_OPTIONS } from './constants/states';
import { Representative } from './types/representatives';
import tw from 'twin.macro';

function App() {
  const [results, setResults] = React.useState<Representative[]>([]);
  const [selectedRep, setSelectedRep] = React.useState<Representative | null>();
  const typeRef = React.useRef<HTMLSelectElement>(null);
  const stateRef = React.useRef<HTMLSelectElement>(null);

  function capitalizeFirst(string: string) {
    return string.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');
  }

  const verifyInput = (type: string, state: string) => {
    if(type === "" || state === "") return false;
    return true;
  }
  

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if(!typeRef.current || !stateRef.current) return;
    const isVerified: boolean = verifyInput(typeRef.current.value, stateRef.current.value);
    if(isVerified){
      const { data } = await axios.get(`/${typeRef.current.value}/${stateRef.current.value}`);
      setResults(data.results);
      setSelectedRep(null);
    } else {
      alert("Type and State are both required.")
    }
  }

  return (
    <div tw="w-full flex flex-col justify-center items-center space-y-8">
      <h1 tw="font-bold mt-10 text-4xl text-blue-400 w-4/6">Who's My Representative?</h1>
      <form onSubmit={handleSubmit} tw="flex flex-row w-3/6 space-x-3">
        <select id="type" defaultValue="" tw="pl-2 form-select block w-full pl-3 pr-10 py-2 text-base border-gray-300 sm:text-sm rounded-md w-full" ref={typeRef}>
          <option value="" disabled hidden>Type</option>
          <option value="senators">Senators</option>
          <option value="representatives">Representatives</option>
        </select>
        <select id="state" defaultValue="" tw="pl-2 form-select block w-full pl-3 pr-10 py-2 text-base border-gray-300 sm:text-sm rounded-md w-full" ref={stateRef}>
          <option value="" disabled hidden>State</option>
          {STATES_OPTIONS.map((state) => {
            return (
              <option value={state.value}>{state.label}</option>
            )
          })}
        </select>
        <button type="submit" tw="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">Search</button>
      </form>
      <div tw="flex flex-row space-x-5 w-4/6">
      {results.length > 0 ? (
        <div tw="w-6/12 space-y-3">
          <h3 tw="text-xl font-normal">List / <span tw="text-blue-400">{capitalizeFirst(typeRef.current?.value || '')}</span></h3>
          <table tw="min-w-full divide-y divide-gray-300 border-collapse" cellSpacing={0}>
            <thead tw="bg-gray-200">
              <tr tw="h-10 bg-gray-200">
                <th tw="font-normal text-left text-gray-500 px-2">Name</th>
                <th tw="font-normal text-left text-gray-500 px-2">Party</th>
              </tr>
            </thead>
            <tbody>
              {results.map((rep: Representative) => {
                return (
                  <tr style={{borderBottom: "1px solid #f3f3f3"}} css={[tw`border-t h-10 hover:bg-blue-400 hover:text-white`, rep === selectedRep && tw`bg-blue-400 text-white`]} onClick={() => {
                    if(rep === selectedRep) {
                      setSelectedRep(null)
                    } else {
                      setSelectedRep(rep)
                    }
                  }}>
                    <td tw="px-2">{rep.name}</td>
                    <td tw="px-2">{rep.party[0]}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      ) : null}
      {selectedRep ? (
        <div tw="w-6/12 space-y-5">
        <h3 tw="text-xl font-normal">Info</h3>
        <div tw="space-y-3">
          <div tw="bg-gray-200 w-full rounded text-gray-700 px-4 py-3">{selectedRep?.name || 'Name'}</div>
          <div tw="bg-gray-200 w-full rounded text-gray-700 px-4 py-3">{selectedRep?.district || 'District'}</div>
          <div tw="bg-gray-200 w-full rounded text-gray-700 px-4 py-3">{selectedRep?.phone || 'Phone'}</div>
          <div tw="bg-gray-200 w-full rounded text-gray-700 px-4 py-3">{selectedRep?.office || 'Office'}</div>
          <div tw="bg-gray-200 w-full rounded text-gray-700 px-4 py-3">
            {<a href={selectedRep?.link} target="_blank">{selectedRep?.link}</a> || 'Link'}
          </div>
        </div>
      </div>
      ) : null}
      </div>
    </div>
  );
}

export default App;
