export type Representative = {
    district: string;
    link: string;
    name: string;
    office: string;
    party: string;
    phone: string;
    state: string;
}